# 使用rxjs写一个小游戏

## 前言

差不多一年前，在我研究redux的时候，看到了rxjs，便开始杠上了rxjs，硬是杠了了几天英文文档，才有个初步了解，后来由于比较忙，又缺少动力，就不了了之了。知道不久前，看到了有人使用rxjs写了个[贪食蛇](https://zhuanlan.zhihu.com/p/35457418)的游戏，才发现原来rxjs可以这么有趣，便有了使用rxjs写微信小游戏的想法。</br>

## 做点什么游戏
相信80后以及90后初代都玩过那种方块掌上游戏机，在那个物质相对贫乏的年代，可以说那游戏机是我们某些方面的启蒙老师，所以我们的目的就是游戏机上面的游戏移植到微信小游戏上面。我们先尝试写一个俄罗斯方块的经典游戏，先看看游戏大概长什么样子的：</br>
<div align=center style='margin:55px 0'>
<img src="https://gitlab.com/ZhaoUjun/my-blogs/raw/master/image/tetris.png" width="400" height="220" alt="亦菲表演机器猫"/>
</div>

## 怎样实现
我们将使用rxjs实现这个小游戏，这里需要你了解rx或者响应式编程，如果之前没有了解过rx或者响应式编程的，建议可以先去[rxjs中文官网](https://cn.rx.js.org/manual/overview.html)了解一下。</br>

### 游戏分析
大家都很熟悉俄罗斯方块了，这里再做简单的描述。如上图，中间区域是游戏显示区域，显示的是正在下落的方块以及已累加的方块堆；左边是方向控制区域，通过点击方向键控制下落方块的方向；右边是功能键区域，主要控制下落方块的旋转。玩家通过控制方块的位置以及旋转来消除累加的方块，从而得到分数，这是这个游戏的核心玩法。</br>

我们知道计算机显示图像是驱动根据某些数据绘制在显示器上面，可以用：view=f(data) 关系来表示。同样，我们在画布上绘制图片时，也是根据数据来绘制的。首先我们把注意力放在中间的游戏显示区域上，这里是一个10*20的小格子组成，每个小格子有两种颜色，要么是深色，要么是浅色，所以，我们可以使用长度为200的数组来表示中间的游戏区：

```ts
    type PlayGround=number[];//其中数组元素表示是第几格处于激活状态（深色）。
```
比如，[5,15,16,17],表示第一行第6个以及第二行第 6、7、8个格子是激化状态，其他则处于未激活状态，如此类推。</br>
进一步继续分解，Playgroud是由正在下落的方块以及累加的方块对组成的
```ts
    type Heap=number[];
    type Tetris=number[];
```
至此，我们可以确定了数据类型，至于具体怎么把数组渲染出来，大家可以看我的源码。
### 溯源
在上面我们初步分析了游戏，但是如何使用rxjs来写呢？在rx中，是通过流的概念表示不同数据之间的关系的，流即数据。如何涌流来确定不同流之间的关系,借鉴函数，g=f(x,y,z...),一个或者一组自变量可以确定一个因变量。在rx中，一个流是另外一个或者一些列流以及之间的关系共同决定的。如在上述中，playground$（通常末尾用$来表示流）是由heap$、tetris$以及他们之间的关系决定的。用伪代码表示

```ts
    const playground$=f(tetris$,heap$)
```
通过溯源，我们可以继续找到tetris$,以及heap$的源头。

```ts
    const tetris$=f(position$,shape$,type$);
    const heap$=f(tetris);
```
其中position$表示位置流，shape$表示方块的旋转状态流，type$表示方块的类型流。</br>
我们继续分解，
```ts
    const position$=f(left$,right$,down$);
    const down$=f(tick$,keyDown$);
    const left$=f(keyLeft$,keyRight$)
```
即位置流由左右下方向以及初始位置决定，其中，下方向除了按键下，还有一个时间流tick$，即使我们不动方向键，方块的位置也会随着事件向下掉。</br>
至此，我们通过溯源法，知道了因变量（流）以及自变量（流），下面我们使用rx提供的方法来描述这些流之间的关系。

## 流及其流之间的关系

### 方向流

方向流position$由left$,right$,down$决定的，而 left$,right$,down$可以是由触摸事件触发的方向流派生的。
```ts
    const touch$=Observable.create(observer=>{
        wx.onTouchStart((res: TouchEvent) => {
            observer.next(res);
        });
    })
    const direction$=new Subject<TouchEvent>();
    const left$ = direction$.filter(filterDiretion("left"));
    const right$ = direction$.filter(filterDiretion("right"));
    const top$ = direction$.filter(filterDiretion("up"));
    const down$ = direction$.filter(filterDiretion("down"));
    touch$.subscribe(direction$);
```
首先使用Observable.create()方法创建一个touch事件流，使用Subject创建一个观察主体direction$流，direction.subscribe()表示direction主题订阅touch事件流，每当touchStart事件触发是，事件的观察者就会发布一个新的数据，direction$进行广播。
这里使用subject经行多播，关于多播概念，建议大家去官网看看具体概念。在这里的主要作用是使上下左右方向流是共用一个执行对象，节省不必要的开销。

### 位置流

每当方块向左移动，方块的位置减去1.向右移动方块的位置加上1，向下移动，方块的位置加上10，
所以这里的关系可以描述为：

```ts
    const left$ = diretion.left$.mapTo(-1);
    const right$ = diretion.right$.mapTo(1);
    const down$ = tick$.merge(diretion.down$).mapTo(10);
    const position$ = down$.merge(left$, right$);
```
这里的mapTo操作符表示把发出的值映射成一个常量，上述把向左流映射为-1.表示向左流发发出的值会被转换为常量-1，其他同理。这里的merge操作符表示同时发出每个给定的输入 Observable 中的所有值，即当left$发出-1时，position$马上发出-1，down$发出10时，position$马上发出10，使用[弹珠语法](https://cn.rx.js.org/manual/usage.html#h27)表示:</br>
```text
left :    ('--a---')
down :    (' ----b----')
right :   ('  ------c-----')
tick :    ('  ---------b--------')
position :('--a--b--c--b--------')

```
### 方块流（tetris$）

上面提到，方块流是由position$,shape$,type$组成的，那么他们之间的关系是怎样的呢？这里首先要说说我们怎样表示一个方块。我们用type表示方块的类型，方块类型共7中，我们可以用一个4*4的格子点阵表示一个方块某种旋转状态，其中其实的起始的格子位置为0，往左右相邻距离为1.上下相差10，如下图</br>
<div align=center style='margin:55px 0'>
<img src="https://gitlab.com/ZhaoUjun/my-blogs/raw/master/image/spin_example.jpg" width="400" height="200" alt="亦菲表演机器猫"/>
</div>

<!-- ![](https://gitlab.com/ZhaoUjun/my-blogs/raw/master/image/spin_example.jpg=200x300)</br> -->
我们暂称这样的数组为方块的元数组，由于我们穷举了7中类型方块所有的可能元数组，对于同一种类型，每次旋转，我们总会知道下一次旋转后的结果。</br>
事实上，我们表示正在下落的方块falingTetris$时，

```ts
const fallingTetris$ = f(position$, currentTetris$, shape$)
```
其中，currentTetris$表示当前方块的类型，这里是由右边下一个方块nextTetris$决定的，下节我们再讨论nextTetris$与currentTetris$具体怎样实现。</br>
每当position$, currentTetris$, shape$中任一个流变化时， fallingTetris$ 都会发生变化，他们之间的关系是这样的：
```ts
const fallingTetris$ = Observable.merge(position$, currentTetris$, shape$)
	.withLatestFrom(heap$, (action, heap) => ({ action, heap }))
	.scan(ensureIsValidAction, { position: 0 })
	.map(createPositions)
	.filter(next => !next.some(i => i > 199))
	.distinctUntilChanged()
	.share();
```
其中这里的Observable.merge方法跟上面的operator方法的作用是一样的。withLatestFrom操作符表示结合另外的元发出的最新的值，生成新的值。事实上每当position$, currentTetris$, shape$中任一个流变化时，fallingTetris$ 不一定会发生变化，比如移动左右时，移动的方向超出边界或者与堆重合都是无效的动作，我们都需要过滤，返回与不变的值。这里就用到scan操作符，通常我们使用scan操作符是用作缓存并计算之前的值，有点像数组的reduce方法。为了去报是有效的动作，我们做这样的逻辑：
```ts
function ensureIsValidAction(acc: TetrisLike, next) {
    //有最新的方块，返回最新的方块，否则若超出边界或者与堆重合都返回之前的值
	const action = next.action;
	const heap = next.heap;
		if (isNext(action)) {
			return { ...action, position: INIT_POSITION };
		} else if (isSpin(action)) {
			const nextTetris = { ...acc, shape: (acc.shape + 1) % shapesLength(acc.type) };
			return isOverBorder(acc, nextTetris) || isOverHeap(nextTetris, heap) ? acc : nextTetris;
		} else {
			const nextTetris = { ...acc, position: acc.position + action };
			return isOverBorder(acc, nextTetris) || isOverHeap(nextTetris, heap) ? acc : nextTetris;
		}
}
```
上面的map，filter操作符都可以类比数组的map以及filter方法。[distinctUntilChanged](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-distinctUntilChanged)操作符会过滤连续相邻两个相同的值，知道发出的值不一样才会继续发出，事实上这里的distinctUntilChanged操作符是不管用的，因为发出的值是个复杂对象，值肯定不相等，这里其实需要用到另外一个操作符[distinctUntilKeyChanged](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-distinctUntilKeyChanged)，这里留个问题，感兴趣的朋友可以研究如何使用这个操作符过滤掉‘相同的’值。[share](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-share)操作符生成多播的Observal。

## 碰撞流
上面讨论了下落的方块的生成，那么什么时候方块会停止，继续下一个方块呢？我们分析可以知道，只有当方块超出底部或者方块跟方块堆碰撞时，才会停止，并且生成下一个方块的同时，方块对合并已经下落的方块。所以我们首先要实现碰撞流：

```ts
const collision$ = tick$
	.withLatestFrom(fallingTetris$, heap$, (_, fallingTetris, heap) => ({ fallingTetris, heap }))
	.filter(checkCollision)

    function checkCollision(playGround: PlayGround): boolean {
	const { fallingTetris, heap } = playGround;
	const nextTetris = fallingTetris.map(i => i + 10);
	return hasSameVal(nextTetris, heap) || nextTetris.some(item => item > 199);
}
```
每次tick$流发出的时候，都检查是否发生碰撞。

## 通知者 nextHeap$
当方块停止时，我们需要做的事情是计算（是否需要消除）并合并下落的方块，并且计算本次消除得分。这里我们使用do操作符，do操作符通常用来处理副作用，这里我们用作通知heap$发出最新的值。

```ts
interface PlayGroundReducer{
	nextHeap:number[]
	score :number
}

function reduceTetris(playGround: PlayGround): PlayGroundReducer {
	const nextHeap = [...playGround.fallingTetris, ...playGround.heap];
	const data = Array(200).fill(0);
	nextHeap.forEach(item => {
		data[item] = 1;
	});
	return generateNextHeap(data.join(""));
}
const nextHeap$ = collision$.map(reduceTetris).do(data => heap$.next(data.nextHeap));
```

 ## 方块流tetris$

 上面我们描述了fallingTetris$由currentTetris$产生，事实上，游戏的右手边还有个正在等待的下一个方块的预告。怎样实现预告呢？答案是缓冲。通过缓冲流发出的前后两个值，我们就可以实现预告的效果。rx有专门缓冲操作符[bufferCount](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-bufferCount)，这里若过使用这个操作符，我们的处理有点麻烦，不过没关系，设计到缓冲的，我们还以可以使用[scan](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-scan)来实现。

 ```ts
const tetris$ = heap$
	.skip(1)
	.map(getRandomTetris)
	.startWith(INIT_TETRIS_2)
	.scan((acc, next) => [acc[1], next], [null, INIT_TETRIS_1])
	.share();

const nextTetris$ = tetris$
	.map(i => i[1])
	.startWith(INIT_TETRIS_2)
	.map(i => ({ ...i, position: 0 }));
const currentTetris$ = tetris$.map(i => i[0]);
```
这里的细节要注意，游戏开始，我们就产生两个方块，为了nextTetris$与tetris$同步，这里都使用了[startWith](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-startWith)操作符，就是因为这个原因，没有使用[bufferCount](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-bufferCount)操作符。</br>

## 分数流score$
上面的碰撞流提到，当碰撞发生消除时，还需要根据碰撞的结果来生成分数。

 ```ts
const score$ = nextHeap$
	.map(i => i.score)
	.withLatestFrom(speed$,calcSocre)
	.scan((acc, next) => acc + next, 0)
	.share()
	.startWith(0)
```

## 速度流speed$
上面分数流树根据消除的情况以及当前速度speed$确定的。细心想想，speed$还影响到方块的下落速度，speed$还是tick$的源头，tick$根据speed$的值变化时钟就发生变化
 ```ts
const speed$=new BehaviorSubject(1);
const tick$ = speed$.mergeMap(speed=>Observable.interval(speed*1000)).share();
```
这里的[BehaviorSubject](https://cn.rx.js.org/class/es6/BehaviorSubject.js~BehaviorSubject.html)是Subject的变种，订阅后总是发出最新的值并且发出一个初始值。这里的tick$使用了[mergeMap](https://cn.rx.js.org/class/es6/Observable.js~Observable.html#instance-method-mergeMapTo)来改变tick$的快慢。

## scene$

至此，我们基本实现了俄罗斯方块的核心数据流动，我们把上述流合并成scene$来表示动画的数据流。
 ```ts
const scene$ = fallingTetris$
	.withLatestFrom(
		heap$,
		score$,
		nextTetris$,
		heightScore$,
		speed$,
		(tetris, heap, score, nextTetris, heightScore, speed) => ({
			tetris,
			heap,
			score,
			nextTetris,
			heightScore,
			speed
		})
	)
	.map(scene => ({ isOver: gameOver(scene), scene }))
	.share();
```

## 游戏合何时结束
当方块堆填充满时，我们认为游戏就结束了
```ts
function gameOver(scene: Scene) {
	const { heap, tetris } = scene;
	return [4, 5, 6].some(item => heap.includes(item));
}
```
## 渲染
为了让动画保持在流畅的动画桢，我们使用[animationFrame](https://cn.rx.js.org/class/es6/Scheduler.js~Scheduler.html)来调度动画帧

```ts
const render$ = Observable.interval(1000 / FPS, animationFrame).share();
const game$ = render$.withLatestFrom(scene$, (_, scene) => scene).takeWhile(scene => !scene.isOver);
```
在没有还优化的情况下，真机（三星S7）运行游戏帧率保持了55到60之间，算是不错了。
## 总结

至此，我们已经实现了俄罗斯方块的核心功能，事实上还有几点没有在这里讨论，如何结束游戏，如何开始游戏，这里都没有讨论，感兴趣的朋友可以看看[源码](https://github.com/ZhaoUjun/rx-mini-game)，也欢迎期待有朋友能用实现完其他几个小游戏。

## 后记
第一次码博客，啰里啰唆的，如有错误或者不当指出，敬请指出。anyway，对我自己而言，已经是踏出了一大步了，也期待后面自己能延续写点东西，共勉~