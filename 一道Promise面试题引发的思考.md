# 一道Promise面试题引发的思考
## 一道Promise面试题说起
上两周跟老大提了要离职的意思，终于开始踏上投简历，跑面试的日子。今天请了个假，去了两家面试，都做了笔试。大部分都是基础，其中有一道关于Promise的面试题，实在想不出来考点是什么，甚至在回家的路上还一直对这道题耿耿于怀，大概长这个样子：

```js
 const promise1=Promise.reject('aaa')
    .then(()=>{},e=>{console.log(1,e)})
    .then(()=>{},e=>{console.log(2,e)})
```
可能是我都比较少用then方法的第二个参数，所以当时一时间对这道题感到陌生。在没认真翻看Promise A+标准之前，我一直以为这道题考的是关于then方法返回的问题，之前粗略看过一下标准，Promise A+中规定了then方法返回一个新的Promise对象，以及受到Promise链式调用的影响，我当时答了大概是onReject也会链式调用，新的Promise也会跟着reject。

## Promise/A+
回家翻看[Promise/A+标准](https://promisesaplus.com/#notes)，认证看标准看了两遍，终于在找到了考点：

```md
2.2.7.1 if either onFulfilled or onRejected returns a value x, run the Promise Resolution Procedure [[Resolve]](promise2, x).
```
也就是说，[Promise/A+标准](https://promisesaplus.com/#notes)规定了，不管是onResovle，还是onReject回调被调用了，都会返回一个Promise，并且值是回调函数返回的值X。这才是考点~ </br>
回到这道题目，要是奔着这个考点，很明显面试题就是不完整的，完整的应该需要这样：
```js
 const promise1=Promise.reject('aaa')
    .then(()=>{},e=>{console.log(1,e);return 'bbb'})
    .then((res)=>{console.log(3,res)},e=>{console.log(2,e)})
```
这样才是考察这个考点的正确姿势。

## 一些思考

我们重新回到刚刚[Promise/A+标准](https://promisesaplus.com/#notes)的规定:
```md
2.2.7.1 if either onFulfilled or onRejected returns a value x, run the Promise Resolution Procedure [[Resolve]](promise2, x).
```
为什么onRejected调用后返回的Promise状态是fullfilled而不是rejected？如果按照规定中的那种，我们在实际中应该怎样使用onRejected回调？</br>
我们不妨假设onRejected调用后返回的Promise状态是rejected，这时，会立即引发onRejected的链式回调。试想一下，如果是引发onRejected的链式回调，那么意味着我们需要维护一套onRejected逻辑。如果是按照标准的那样规定，我们只需要在onResovled回调处理这个逻辑就好，这样就降低了链式流的复杂度。</br>
打个比方，现在需要展示用户的一些信息，请求一个获取用户信息的接口，但是获取用户信息之前一定需要先调用登陆接口，调用登陆前需要调用获取code接口，伪代码大概是这样：
```js
 getCode()
 .then(login,onGetCodeFail)
 .then(getUserInfo,onLoginFail)
 .then(displayUserInfo,onGetUserInfoFail)
```
如果onRejected是链式调用，那么会分别一次调用onGetCodeFail、onLoginFail、onGetUserInfoFail，我们需要在者三个回调中都设置相应的错误处理逻辑，这样就增加了应用的复杂度了。</br>
其实我觉得目前最好的办法是，在尾部添加.catch方法是最优雅的方案：
```js
 getCode()
 .then(login)
 .then(getUserInfo)
 .then(displayUserInfo)
 .catch(handleError)
```
这样catch能抓到任何错误，我们可以在catch里面同意处理异常，与上面的代码比较，代码的明显清晰易懂了。同时，在过程中还能避免一些无关的逻辑判断，比如上述的，如果获取code失败，完全是没必要调用login的，同理getuserInfo如此。</br>
综上，为了代码的简洁优雅，能在catch里面统一处理错误的，最好不要在onRejected回调处理。


